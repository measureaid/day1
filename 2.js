// var util = require('util');

var process1 = function(callback) {
    setTimeout(function() {
        var result = (Math.floor(Math.random() * 10) > 5) ? null : new Error('Process1Error');
        console.log(result, 'PROCESS 1 END');
        callback(result, 'PROCESS 1 END');
    }, 200);
};
var process2 = function(callback) {
    setTimeout(function() {
        var result = (Math.floor(Math.random() * 10) > 5) ? null : new Error('Process2Error');
        console.log(result, 'PROCESS 2 END');
        callback(result, 'PROCESS 2 END');
    }, 200);
};
var process3 = function(callback) {
    setTimeout(function() {
        var result = (Math.floor(Math.random() * 10) > 5) ? null : new Error('Process3Error');
        console.log(result, 'PROCESS 3 END');
        callback(result, 'PROCESS 3 END');
    }, 200);
};
var process4 = function(callback) {
    setTimeout(function() {
        var result = (Math.floor(Math.random() * 10) > 5) ? null : new Error('Process4Error');
        console.log(result, 'PROCESS 4 END');
        callback(result, 'PROCESS 4 END');
    }, 200);
};
var process5 = function(callback) {
    setTimeout(function() {
        var result = (Math.floor(Math.random() * 10) > 5) ? null : new Error('Process5Error');
        console.log(result, 'PROCESS 5 END');
        callback(result, 'PROCESS 5 END');
    }, 200);
};


var fromProcess1 = function fromProcess1(err, result, callback) {
    process1(function(err_1, result_1) {
        if (err_1) {
            fromProcess1(null, null, callback);
        } else {
            fromProcess2(null, null, callback);
        }
    });
};

var fromProcess2 = function fromProcess2(err, result, callback) {
    process2(function(err_2, result_2) {
        if (err_2) {
            fromProcess2(null, null, callback);
        } else {
            fromProcess3(null, null, callback);
        }
    });
};
var fromProcess3 = function fromProcess3(err, result, callback) {
    process3(function(err_3, result_3) {
        if (err_3) {
            fromProcess3(null, null, callback);
        } else {
            fromProcess4(null, null, callback);
        }
    });
};
var fromProcess4 = function fromProcess4(err, result, callback) {
    process4(function(err_4, result_4) {
        if (err_4) {
            fromProcess4(null, null, callback);
        } else {
            fromProcess5(null, null, callback);
        }
    });
};
var fromProcess5 = function fromProcess5(err, result, callback) {
    process5(function(err_5, result_5) {
        if (err_5) {
            fromProcess5(null, null, callback);
        } else {
            callback(err_5, "FINISHED");
        }
    });
};

//1 ~ 5
fromProcess1(null, null, function(err_1, result_1) {
    console.log(err_1, result_1, 'END ALL');
});
