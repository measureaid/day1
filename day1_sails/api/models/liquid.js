module.exports = {
  attributes : {
    name:'STRING',
    nicotine:'INTEGER',


    flavor: {
      collection: 'flavor',
      through: 'liquid_has_flavor'
    }
  },
};
