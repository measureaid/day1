module.exports = {
  attributes: {
    name:'STRING',
    liquid: {
      collection: 'liquid',
      through: 'liquid_has_flavor',
    }
  }
};
