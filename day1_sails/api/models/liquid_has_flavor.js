module.exports = {
  attributes: {
    asdf : 'string', //additional attributes can be added

    liquid:{
      type: 'integer',
      foreignKey: true,
      references: 'liquid',
      on: 'id',
      onKey: 'id',
      via: 'flavor'
    },
    flavor:{
      type: 'integer',
      foreignKey: true,
      references: 'flavor',
      on: 'id',
      onKey: 'id',
      via: 'liquid'
    }
  }
};
