var Sails = require('sails'),
  sails;
var request = require('supertest');

before(function(done) {

  // Increase the Mocha timeout so that Sails has enough time to lift.
  this.timeout(20000);

  Sails.lift({
    // configuration for testing purposes
  }, function(err, server) {
    sails = server;
    if (err) {
      return done(err);
    }
    done(err, server);
  });
});
