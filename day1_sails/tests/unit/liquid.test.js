var request = require('supertest');
var expect = require('expect.js');
var liquid1Id;
var flavor1Id;
/* globals describe, it, sails, liquid, _, liquid_has_flavor*/
describe('Liquid 가 추가될 수 있어야 함', function() {
  it('Liquid 추가', function(done) {
    request(sails.hooks.http.app)
      .post('/liquid')
      .send({
        name: '태영액상',
        nicotine: 18,
      })
      .end(function(err, result) {
        // console.log(result.body);
        liquid1Id = result.body.id;
        done(err);
      });
  });
  // it('추가', function () {
  //   liquid.create({
  //     name: '태영액상',
  //     nicotine: 18,
  //   }).exec(function (err, result) {
  //
  //   });
  // });
  //
  it('Liquid 갯수가 1개', function(done) {
    request(sails.hooks.http.app)
      .get('/liquid')
      .end(function(err, result) {
        // console.log(result.body);
        expect(result.body.length).to.be(1);
        done(err);
      });
    // console.log(liquid);
    // liquid.find().exec(function(err, liquids) {
    //   // console.log(liquids);
    //   expect(liquids.length).to.be(1);
    //   done(err);
    // });
  });
  it('liquidId1 의 이름이 태영액상이어야 한다.', function(done) {
    request(sails.hooks.http.app)
      .get('/liquid/' + liquid1Id)
      .end(function(err, result) {
        // console.log(result.body);
        expect(result.body.name).to.be('태영액상');
        expect(result.body.nicotine).to.be(18);
        expect(result.body.nicotine).not.to.be(8);
        done(err);
      });
    // liquid.findOne({
    //   id: liquid1Id
    // }).exec(function(err, _liquid) {
    //   expect(_liquid.name).to.be('태영액상');
    //   expect(_liquid.nicotine).to.be(18);
    //   expect(_liquid.nicotine).not.to.be(8);
    //   done(err);
    // });
  });
  it('Liquid 가 수정될 수 있어야 한다.', function(done) {
    request(sails.hooks.http.app)
      .put('/liquid/' + liquid1Id)
      .send({
        name: '태영액상(수정됨)',
      })
      .end(function(err, result) {
        // console.log(result.body);
        // liquid1Id = result.body.id;
        done(err);
      });

  });
  it('Liquid id 1의 이름이 수정되어야 한다.', function(done) {
    liquid.findOne({
      id: liquid1Id
    }).exec(function(err, _liquid) {
      expect(_liquid.name).to.be('태영액상(수정됨)');
      expect(_liquid.nicotine).to.be(18);
      expect(_liquid.nicotine).not.to.be(8);
      done(err);
    });
  });

  it('whrjswhlsfds', function(done) {
    done();
  });

  it('flavor가 추가될 수 있어야 한다', function(done) {
    request(sails.hooks.http.app)
      .post('/flavor')
      .send({
        name: '담배',
      })
      .end(function(err, result) {
        // console.log(result);
        flavor1Id = result.body.id;
        done(err);
      });
  });

  it('태영액상은 담배향을 가질 수 있다.', function(done) {
    request(sails.hooks.http.app)
      .post('/liquid/' + liquid1Id + '/flavor')
      .send({
        liquid: liquid1Id,
        flavor: flavor1Id,
      })
      .end(function(err, result) {
        // console.log(result);
        // flavor1Id = result.body.id;
        done(err);
      });
  });
  //
  // it('태영액상은 담배향을 가지고 있다.', function(done) {
  //   liquid.findOne({
  //       id: liquid1Id
  //     })
  //     .populate('flavor')
  //     .exec(function(err, _liquid) {
  //       console.log(_liquid.flavor);
  //       // var i=0;
  //       // var find_result = false;
  //       // for(i=0;i<_liquid.flavor.length;i++){
  //       //   if(_liquid.flavor[i].flavor === flavor1Id){
  //       //     find_result = true;
  //       //   }
  //       // }
  //       expect(_.find(_liquid.flavor, {
  //         flavor: flavor1Id
  //       })).not.to.be(undefined);
  //
  //       // expect(find_result).to.be(true);
  //       // expect(_liquid.name).to.be('태영액상(수정됨)');
  //       // expect(_liquid.nicotine).to.be(18);
  //       // expect(_liquid.nicotine).not.to.be(8);
  //       done(err);
  //     });
  // });
  it('태영액상은 담배향을 가지고 있다.', function(done) {
    liquid_has_flavor.find({
      liquid: liquid1Id,
      flavor: flavor1Id
    }).exec(function(err, result) {
      console.log(result);
      expect(result.length).to.be(1);
      done(err);
    });
  });
});
