
//Prototype

status = "gone";

var f = function (str) {
    this.status = str;
};

f.prototype.get_status = function () {
    return this.status;
};

var myf = new f("going");

console.log(myf.get_status());
 // "going"

var myf2 = f('going');
console.log(myf2.get_status());
