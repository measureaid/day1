// function User(n) {
//     this.name = n;
//
//     this.hello = function () {
//       console.log('Hi, I\'m ' + this.name);
//     };
// }
// var bob = new User('Bob');
// User.prototype.sayHi = function() {
//     console.log('Hi, I\'m ' + this.name);
// };
// bob.sayHi(); // "Hi, I'm Bob"


// 실행 순서 - 동기와 비동기

//비동기 함수 => Callback에 의한 제어
var longtime = function (callback) {
  // delay(1000);
  setTimeout(function () {
    console.log('Long time Ended');
    callback(null, "Ended");
  },1000);
};

console.log('Start');
longtime(function (err, result) {
  console.log(err, result, "REAL END");
});
console.log('Ended?');

//
//
// 1
// 2
// 3
// 4
