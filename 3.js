// // console.log('콜백');
//
// function sum(arg1, arg2){
//   // var ret = arg1 + arg2;
//   // callback(null, ret);
//   return arg1 + arg2;
// }
//
// console.log(sum);
//
// // 1+2+3;
//
// console.log(sum(1,2));
//
// console.log(sum.apply(null, [1,2]));
//
// // sum(1,2, function (err, result) {
// //  // console.log(result);
// //  sum(result, 3, function (err, final_result) {
// //    console.log(final_result);
// //  });
// // });


var o1 = {
    val1: 1,
    val2: 2,
    val3: 3
};
var o2 = {
    v1: 10,
    v2: 50,
    v3: 100,
    v4: 25
};
//This

function sum() {
    var _sum = 0;
    for (name in this) {
        _sum += this[name];
    }
    return _sum;
}

//apply Array
//call Argument

console.log(sum.apply(o1)) // 6
console.log(sum.apply(o2)) // 185
