var express = require('express');
var app = express();

//Node.js 의 Express 프레임워크

app.get('/', function (req, res) {
  res.send('<a href="/hello"><h1>Hello World!</h1></a>');
});
app.get('/hello', function (req, res) {
  res.send('Hello World!!!!!!!!!');
});
var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
