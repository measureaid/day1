var async = require('async');

var process1 = require('./submodules/process1.js');
console.log(process1);

var process2 = require('./submodules/process2');
var process3 = require('./submodules/process3');
var process4 = require('./submodules/process4');
var process5 = require('./submodules/process5');

var data = [1, 2, 3, 4, 5, 6, 7, 8];

async.series(
    [
        function(callback) {
            process1(function(err_1, result_1) {
                async.retry(20, process1, function (err_retry, retry_result) {
                    console.log(err_retry, retry_result);
                    callback(err_retry, retry_result);
                });
            });
            // console.log(callback);
            // callback();
        },
        function(callback) {
            console.log(callback);
            process2(function (err, result) {
              callback(err, result);
            });
        },
        function(callback) {
            process3(function(err_3, result_3) {
                async.retry(20, process3, callback);
            });
      },
        function(callback) {
            process4(function(err_4, result_4) {
                async.retry(20, process4, callback);
            });
        },
        function(callback) {
            process5(function(err_5, result_5) {
                async.retry(20, process5, callback);
            });
        },

    ],
    function(err, results) {
        console.log(err, results);
    }
);
