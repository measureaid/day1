// function cal(func, num){
//     return func(num);
// }
// function increase(num){
//     return num+1;
// }
// function decrease(num){
//     return num-1;
// }
//
// console.log(cal(increase, 1));
// console.log(cal(decrease, 1));

// function cal(mode) {
//     var funcs = {
//         'plus': function(left, right) {
//             return left + right;
//         },
//         'minus': function(left, right) {
//             return left - right;
//         }
//     };
//     return funcs[mode];
// }

// Array 와 Object
// a = [1, 2, 3, 4];
// b = {
//     a: 2,
//     b: 3,
//     c: 4
// };

// alert(cal('plus')(2,1));
// alert(cal('minus')(2,1));

function sortNumber(a,b){
    // 위의 예제와 비교해서 a와 b의 순서를 바꾸면 정렬순서가 반대가 된다.
    return a-b;
}
var numbers = [20, 10, 9,8,7,6,5,4,3,2,1];
console.log(numbers.sort(sortNumber));
// array, [20,10,9,8,7,6,5,4,3,2,1]
