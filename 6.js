a = 'global this';
// console.log(a);
// console.log(this.a);

  function test() {
    console.log('1',this);
      this.a = 'that this';

      (function test2() {
        console.log('2',this);

          // console.log(this);
          console.log(this.a);
      })();
      console.log('3',this);

      // var _test2 = test2.bind(this);
      //global this 가 찍힌다.
      // test2();
      // setTimeout(test2,1);

      //아래 2개는 모두 같은 결과
      // test2.apply(this);
      //_test2();

      //that this 가 찍힌다.
      //타이머같은 콜백 함수로도 응용 가능함
      // setTimeout(_test2,1);

  }

  var new_scope = {
    a: 'local this'
  };

  //bind context
  var binded_test = test.bind(new_scope);
  binded_test();

// var obj = new test();
//
// // test;
//
//
// var a = function() {};
// a.b = function() {
//     console.log(this);
// };
//
// var a = function() {
//     b: function() {
//         console.log(this);
//     };
// }
//
//
//
//

var oj = {
    method: function() {
        // function inner() {
        //     console.log(this); // window
        // }
        console.log(this); // window

        (function() {
            console.log('INNER THIS', this); // window
        })();
    }
};
