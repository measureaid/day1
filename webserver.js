var http = require('http');

//Node.js 의 http 모듈

http.createServer(
  function (req, res) {
    // console.log('Created', req, res);
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.write("HELLO");
    res.end();
  }
).listen(8088);
