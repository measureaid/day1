// var vscope = 'global';
// function fscope(){
//     var vscope = 'local';
//     console.log('INSIDE', vscope);
// }
//
// console.log('OUTSIDE', vscope);
// fscope();
// console.log('OUTSIDE', vscope);

// var i;
//
// function r() {
//     console.log(i);
// }
//

// var process1 = function(callback) {
//   debugger;
//     setTimeout(function() {
//         var result = (Math.floor(Math.random() * 10) > 5) ? null : new Error('Process1Error');
//         console.log(result, 'PROCESS 1 END');
//         callback(result, 'PROCESS 1 END');
//     }, 200);
// };

// 변수의 스코프 - 실행 시점에 결정
var fs=[];

fs = [
  // function(){
  //   console.log('1' + ' Function');
  // },
  // function(){
  //   console.log('2' + ' Function');
  // }
];
var i;
for(i = 0; i<10; i++){
  var generate = function (j) {
    fs.push(function () {
      console.log(j + ' Function');
    });
  };

  generate(i);
}

console.log(fs[0]);
console.log(this);
fs[0]();
fs[1]();

//
// for (i = 0; i < 10; i++) {
//     r();
// }
